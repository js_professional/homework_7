const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = merge(common, {
    output: {
        filename: 'js/bundle.[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
    },
    mode: 'production',
    stats: {
        colors: true
    },
    optimization: {
        minimize: true,
        minimizer: [
            new OptimizeCssAssetsPlugin(),
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true
                    }
                }
            })
        ],
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /node_modules/,
                    name: 'vendors',
                    chunks: 'all'
                },
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [
                    'html-loader'
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: "./css/[name].[hash].css"
        }),
        new HTMLWebpackPlugin({
            title: "Task1 HTML",
            filename: 'task1.html',
            template: './src/assets/task1.html',
            inject: 'body',
            chunks: [
                'task1'
            ]
        }),
        new HTMLWebpackPlugin({
            title: "Task2 HTML",
            filename: 'task2.html',
            template: './src/assets/task2.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: true
            },
            chunks: [
                'task2'
            ]
        })
    ]
});

