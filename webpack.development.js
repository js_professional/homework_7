const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
    output: {
        filename: 'bundle.[name].[hash].js',
        path: path.resolve(__dirname, 'public'),
    },
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        port: 8080,
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [
                    'html-loader'
                ]
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            title: "Task1 HTML",
            filename: 'task1.html',
            template: './src/assets/task1.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true
            },
            chunks: [
                'task1'
            ]
        }),
        new HTMLWebpackPlugin({
            title: "Task2 HTML",
            filename: 'task2.html',
            template: './src/assets/task2.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true
            },
            chunks: [
                'task2'
            ]
        })
    ]
});

