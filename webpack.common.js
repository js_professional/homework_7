module.exports = {
    entry: {
        task1: './src/task1.js',
        task2: './src/task2.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ]
                    }
                },
                exclude: /(node_modules)/,
            },
        ]
    }
}
