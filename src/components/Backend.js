export class Backend {

    constructor({ _id, name, type, rate }){
        this._id = _id;
        this.name = name;
        this.type = type;
        this.rate = rate;
    }

    render( target ) {

        let tr = document.createElement('tr');
        let td1 = document.createElement('td');
        let td2 = document.createElement('td');
        let td3 = document.createElement('td');

        td1.setAttribute('class', 'empl');
        td1.setAttribute('data-id', this._id);

        td2.setAttribute('class', 'empl');
        td3.setAttribute('class', 'empl');

        let text1 = document.createTextNode(this.name);
        let text2 = document.createTextNode(this.type);
        let text3 = document.createTextNode(`$${this.rate.toFixed(2)}/h`);

        td1.appendChild(text1);
        td2.appendChild(text2);
        td3.appendChild(text3);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);

        target.appendChild(tr);
    }

}
