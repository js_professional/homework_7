const TrafficLight = () => {

    const start = new CustomEvent('start', {
        detail: {
            color: 'green'
        }
    });

    const stop = new CustomEvent('stop', {
        detail: {
            color: 'red'
        }
    });

    const start_night = new CustomEvent('start_night', {
        detail: {
            color: 'yellow'
        }
    });

    const lights = document.querySelectorAll('.trafficLight');

    const startAll = (items) => {
        items.forEach(item => {
            if (item.color !== 'yellow') {
                item.color = 'yellow';
                item.dispatchEvent(start_night);
            }
        });
    };


    lights.forEach(item => {

        item.addEventListener('start', event => {
            item.color = 'green';
            event.target.classList = `trafficLight ${event.detail.color}`;
        });

        item.addEventListener('stop', event => {
            item.color = 'red';
            event.target.classList = `trafficLight ${event.detail.color}`;

        });

        item.addEventListener('start_night', event => {
            item.classList = `trafficLight ${event.detail.color}`;
            item.timer = setInterval(() => item.classList.toggle(event.detail.color), 1000);

        });


        item.color = 'yellow';
        item.dispatchEvent(start_night);

        item.addEventListener('click', () => {
            switch (item.color) {
                case 'yellow':
                    item.color = 'red';
                    item.dispatchEvent(stop);
                    clearInterval(item.timer);
                    break;
                case 'red':
                    item.color = 'green';
                    item.dispatchEvent(start);
                    break;
                case 'green':
                    item.color = 'green';
                    item.dispatchEvent(start);
                    break;
                default:
                    item.color = 'yellow';
                    item.dispatchEvent(start_night);
            }
        });

    });

    document.querySelector('#Do').addEventListener('click', () => {
        startAll(lights);
    });

};


export default TrafficLight;
