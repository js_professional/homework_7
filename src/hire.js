import {
    Backend, Frontend, Project, Design
} from './components';


const fetchText = () => {
    return fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
        .then( response => response.json());
};


const HeadhuntFactory = (obj) => {
    if (obj.type) {
        let classElement = components.find( item => obj.type === item.type);
        if (classElement) {
            return new classElement.component(obj);
        }
    }
};

const components = [
    {type: "backend", component: Backend},
    {type: "frontend", component: Frontend},
    {type: "project", component: Project},
    {type: "design", component: Design},

];


const hire = () => {

    fetchText()
        .then( data => {
            let tbl = document.querySelector('#leftTable');
            let tbody = document.createElement('tbody');

            tbl.appendChild(tbody);
            tbody.innerHTML = render(data);

            document.querySelectorAll('.buttons').forEach( items => {
                items.addEventListener("click", event => {
                    let _id = event.target.dataset.id;
                    let employee = data.find( item => item._id === _id );

                    const rightTable = document.querySelector('#rightTable');
                    let emplClass = document.querySelectorAll('.empl');

                    let counter = HeadhuntFactory( employee );
                    let ids = [];
                    if ( emplClass.length !== 0 ) {
                        emplClass.forEach( elem  => {
                            if ( elem.dataset.id === _id ) {
                                ids.push(_id);
                            }
                        })
                    }
                    let exist = ids.find( item => item === _id );

                    !exist ? counter.render(rightTable) : alert (`Сотрудник:  ${employee.name} уже нанят`);
                });
            });
        })
        .catch( error => {
            console.log('Error message: ', error);
        });
};

const render = (data) => {
    return data.map((item, id) => {
        return (
            `<tr>
                 <td>${++id}</td>
                 <td>${item.name} (${item.age})</td>
                 <td>${item.type}</td>
                 <td>
                    <button class="buttons" data-id="${item._id}">Нанять</button>
                 </td>
              </tr>`
        )
    }).join('');
};


export default hire;
